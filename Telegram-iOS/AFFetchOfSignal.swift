//
//  AFFetchOfSignal.swift
//  _idx_AccountContext_1A867DAB_ios_min11.0
//
//  Created by Худышка К on 27.11.2022.
//

import Foundation
import SwiftSignalKit

protocol AFFetchOfSignalProtocol {
    func fetch() -> Signal<AFTimezone?, NoError>
}

final class FetchAPIManagerImpl: AFFetchOfSignalProtocol {
    func fetch() -> Signal<AFTimezone?, NoError> {
        let promise = Promise<AFTimezone?>(nil)
        
        AFApiTimezone.getTimezone { value in
            promise.set(Signal { subscriber in
                subscriber.putNext(value)
                subscriber.putCompletion()
                return EmptyDisposable
            })
        }
        
        return promise.get()
    }
}
